#include "GameRun.h"

GameRun::GameRun() {
	m_videoGL.videoInit();
	m_videoGL.enableBlend();
	m_videoGL.enableCulling();

	FGui::Init(m_videoGL.getWindow());
}

void GameRun::MainScene() {
	FLogs::shared_instance().Log("STARING MAIN SCENE");

	FCamera camera = FCamera();
	Shader shader01 = Shader(PATH_DEFAULT_VERTEX_SHADER.c_str(), PATH_DEFAULT_FRAG_SHADER.c_str());
	FTime time = FTime();
	FInput input = FInput(m_videoGL.getWindow());
	input.SetInputExecutors(&camera);

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();
	FColor dimWhite = { white.r * 0.2f,white.g * 0.2f,white.b * 0.2f, 1.0f };

    std::string cubePath = PATH_MESH_CUBE;
    std::string floorPath = PATH_MESH_FLOOR;
    std::string backPack = PATH_MESH_BACKPACK;

	FModel fmodel = FModel(cubePath, false);
	IModel* model = fmodel.Model();

	float cubeSize = 1.0f;	

	glm::vec3 dirLightPos = glm::vec3(1.0f, 0.0f, 3.0f);
	FLight dirLight = FLight(FLightType::DIRECTIONAL);

	while (!glfwWindowShouldClose(m_videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		input.PollEvents();
		InputCloseGame(&input);
		//InputCameraMove(&camera, &input, deltaTime);
		InputJoyCameraMove(&camera, &input, deltaTime);
		InputActivateMouse(&input, &camera);

		FGui::FrameStart(&input);
		FGui::ExampleWindow(&cubeSize, &dimWhite, &dirLightPos);

		glm::vec3 cubePos = glm::vec3(0.0f, 0.0f, -1.0f);
		glm::vec3 cubevSize = glm::vec3(cubeSize);
		
		model->SetPosition(&cubePos);
		model->SetScale(&cubevSize);

		dirLight.Calculate(&shader01, &dimWhite, &dirLightPos);
		m_videoGL.clearFrame();
		camera.render(shader01);
		
		model->DrawModel(shader01);

		glm::vec3 cubePos2 = glm::vec3(5.0f, 0.0f, 4.0f);
		glm::vec3 cubevSize2 = glm::vec3(cubeSize);

		model->SetPosition(&cubePos2);
		model->SetScale(&cubevSize2);
		model->DrawModel(shader01);

		FGui::Render();
		m_videoGL.endFrame();
	}

	FGui::Terminate();
	m_videoGL.terminate();
}

void GameRun::InputCameraMove(FCamera* p_camera, FInput* p_input, float p_deltaTime) {
	if (!m_mouseEnabled) {
		return;
	}

	float xLook = 0.0f;
	float yLook = 0.0f;

	if (p_input->GetInput(GLFW_KEY_W, m_videoGL.getWindow())) {
		p_camera->moveForward(p_deltaTime);
	}
	if (p_input->GetInput(GLFW_KEY_S, m_videoGL.getWindow())) {
		p_camera->moveBackwards(p_deltaTime);
	}
	if (p_input->GetInput(GLFW_KEY_A, m_videoGL.getWindow())) {
		p_camera->moveLeft(p_deltaTime);
	}
	if (p_input->GetInput(GLFW_KEY_D, m_videoGL.getWindow())) {
		p_camera->moveRight(p_deltaTime);
	}

	if (p_input->GetInput(GLFW_KEY_UP, m_videoGL.getWindow())) {
		yLook = -1.0f;
	}
	else if (p_input->GetInput(GLFW_KEY_DOWN, m_videoGL.getWindow())) {
		yLook = 1.0f;
	}
	else if (p_input->GetInput(GLFW_KEY_RIGHT, m_videoGL.getWindow())) {
		xLook = 1.0f;
	}
	else if (p_input->GetInput(GLFW_KEY_LEFT, m_videoGL.getWindow())) {
		xLook = -1.0f;
	}
	
	p_camera->moveCameraView(xLook, yLook);
}

void GameRun::InputJoyCameraMove(FCamera* p_camera, FInput* p_input, float p_deltaTime) {
	glm::vec2 rightJoyAxis = p_input->GetStickAxis(GLFW_GAMEPAD_AXIS_RIGHT_X, GLFW_GAMEPAD_AXIS_RIGHT_Y);
	p_camera->moveCameraView(rightJoyAxis.x, rightJoyAxis.y);

	glm::vec2 leftJoyAxis = p_input->GetStickAxis(GLFW_GAMEPAD_AXIS_LEFT_X, GLFW_GAMEPAD_AXIS_LEFT_Y);
	p_camera->moveXAxis(leftJoyAxis.x * p_deltaTime);
	p_camera->moveZAxis(leftJoyAxis.y * p_deltaTime);
}

void GameRun::InputCloseGame(FInput* p_input) {
	if (p_input->GetInput(GLFW_KEY_ESCAPE, m_videoGL.getWindow()) || p_input->GetButtonDown(GLFW_GAMEPAD_BUTTON_BACK)) {
		CloseGame();
	}
}

void GameRun::InputActivateMouse(FInput* p_input, FCamera* p_camera) {
	if (p_input->GetInputDown(GLFW_KEY_SPACE)) {
		m_mouseEnabled = !m_mouseEnabled;
		m_videoGL.changeMouseVisibility(m_mouseEnabled);
		p_camera->setMovement(m_mouseEnabled);
	}
}

void GameRun::CloseGame() {
	m_videoGL.closeWindow();
}
