#include "FModel.h"

FModel::FModel(std::string path, bool p_flipTexure) {
#if defined(_M_X64) || defined(__amd64__)
	m_model = new AssimpModel(path, p_flipTexure);
#else
	m_model = new TinyModel(path, p_flipTexure);
#endif
}

FModel::~FModel() {
	delete m_model;
}

IModel* FModel::Model() {
	return m_model;
}