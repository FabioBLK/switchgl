#pragma once
#include "FLogs.h"
#include "AppRun.h"
#include "GameRun.h"

#if defined __SWITCH__
#include <switch.h>
#endif

int main() {
#if defined __SWITCH__	
	Result fsResult = romfsInit();
	if (R_FAILED(fsResult))
	{
		printf("Error loading romfs");
		return -1;
	}
#endif

	FLogs::shared_instance().Log("STARTING APP");
	//AppRun app = AppRun();
	//app.GettingStartedApp();
	//app.Lighting();
	//app.Models();
	//app.BlendTests();
	//app.CubeMapTest();
	//app.AdvancedGLSL();
	//app.UniformBufferTest();
	//app.GeometryShaderInit();
	//app.NormalDebugModels();
	//app.InstancingTest();
	//app.AsteroidBelt();
	//app.AsteroidBeltInstance();

	GameRun game = GameRun();
	game.MainScene();

#if defined __SWITCH__
	romfsExit();
#endif

	return 0;
}