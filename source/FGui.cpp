#include "FGui.h"

void FGui::Init(GLFWwindow* p_window) {
	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsClassic();

	// Setup Platform/Renderer backends
	ImGui_ImplGlfw_InitForOpenGL(p_window, true);
	const char* glsl_version = "#version 430 core";
	ImGui_ImplOpenGL3_Init(glsl_version);
}

void FGui::FrameStart(FInput* p_input) {
	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	
	ImGuiIO& io = ImGui::GetIO();
    memset(io.NavInputs, 0, sizeof(io.NavInputs));

    // Update gamepad inputs
    #define MAP_BUTTON(NAV_NO, BUTTON_NO)       { if (p_input->GetButtonDown(BUTTON_NO)) io.NavInputs[NAV_NO] = 1.0f; }
    MAP_BUTTON(ImGuiNavInput_Activate,   GLFW_GAMEPAD_BUTTON_A);     // Cross / A
    MAP_BUTTON(ImGuiNavInput_Cancel,     GLFW_GAMEPAD_BUTTON_B);     // Circle / B
    MAP_BUTTON(ImGuiNavInput_Menu,       GLFW_GAMEPAD_BUTTON_X);     // Square / X
    MAP_BUTTON(ImGuiNavInput_Input,      GLFW_GAMEPAD_BUTTON_Y);     // Triangle / Y
    MAP_BUTTON(ImGuiNavInput_DpadLeft,   GLFW_GAMEPAD_BUTTON_DPAD_LEFT);    // D-Pad Left
    MAP_BUTTON(ImGuiNavInput_DpadRight,  GLFW_GAMEPAD_BUTTON_DPAD_RIGHT);    // D-Pad Right
    MAP_BUTTON(ImGuiNavInput_DpadUp,     GLFW_GAMEPAD_BUTTON_DPAD_UP);    // D-Pad Up
    MAP_BUTTON(ImGuiNavInput_DpadDown,   GLFW_GAMEPAD_BUTTON_DPAD_DOWN);    // D-Pad Down
    MAP_BUTTON(ImGuiNavInput_FocusPrev,  GLFW_GAMEPAD_BUTTON_LEFT_BUMPER);     // L1 / LB
    MAP_BUTTON(ImGuiNavInput_FocusNext,  GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER);     // R1 / RB
    MAP_BUTTON(ImGuiNavInput_TweakSlow,  GLFW_GAMEPAD_BUTTON_LEFT_BUMPER);     // L1 / LB
    MAP_BUTTON(ImGuiNavInput_TweakFast,  GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER);     // R1 / RB
    #undef MAP_BUTTON
	io.BackendFlags |= ImGuiBackendFlags_HasGamepad;

	ImGui::NewFrame();
}

void FGui::Render() {
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
void FGui::Terminate() {
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

void FGui::ExampleWindow(float* p_cubeSize, FColor* p_lightColor, glm::vec3* p_position) {
	//ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
	//bool show_demo_window = true;
	//ImGui::ShowDemoWindow(&show_demo_window);

	static int counter = 0;
	ImGuiWindowFlags window_flags = 0;
	window_flags |= ImGuiWindowFlags_NoMove;
	window_flags |= ImGuiWindowFlags_NoResize;

	ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

	ImGui::Text("Change Cube color and size");               // Display some text (you can use a format strings too)

	ImGui::SliderFloat("float", p_cubeSize, 1.0f, 2.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
	ImGui::ColorEdit3("light color", &p_lightColor->r); // Edit 3 floats representing a color
	ImGui::InputFloat3("input float3", &p_position->x);

	if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
		counter++;
	ImGui::SameLine();
	ImGui::Text("counter = %d", counter);

	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::End();
}
