#pragma once
#include "FInput.h"
#include "Colors.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include <glm/glm.hpp>

namespace FGui
{
	void Init(GLFWwindow* p_window);
	void FrameStart(FInput* p_input);
	void Render();
	void Terminate();
	void ExampleWindow(float* p_cubeSize, FColor* p_lightColor, glm::vec3* p_position);
};

