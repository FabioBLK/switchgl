#pragma once
#include "IInput.h"
#include "FLogs.h"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <iostream>
#include <unordered_set>
#include <vector>

class FInput
{
public:
	FInput(GLFWwindow* p_window);
	void SetInputExecutors(IInput* p_input);
	void RemoveInputExecutors(IInput* p_input);
	bool GetInput(int key, GLFWwindow* p_window);
	bool GetInputDown(int key);
	void PollEvents();
	bool GetButton(int key);
	bool GetButtonDown(int key);
	float GetAxis(int p_axis);
	glm::vec2 GetStickAxis(int p_horizontalAxis, int p_verticalAxis);
	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	void mouse_callback(GLFWwindow* window, double xpos, double ypos);
	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

private:
	std::vector<IInput*> m_inputList;
	std::unordered_set<int> m_pressedKeys;
	GLFWgamepadstate m_lastState;
	GLFWgamepadstate m_currentState;

	const float m_joyDeadZone = 0.25f;
};