#pragma once
#include "Shader.h"

class IModel
{
public:
	virtual void DrawModel(Shader& shader) = 0;
	virtual void Draw(Shader& shader) = 0;
	virtual void DrawInstanced(Shader& shader, const unsigned int p_amount) = 0;
	virtual void SetInstances(const unsigned int p_amount, glm::mat4* p_matrices) = 0;
	virtual void SetPosition(const glm::vec3* p_position) = 0;
	virtual void SetScale(const glm::vec3* p_scale) = 0;
	virtual ~IModel();
};

