#include "Colors.h"

namespace Colors {
	FColor Yellow() {
		const FColor result = { 1.0f, 1.0f, 0.0f, 1.0f };
		return result;
	}

	FColor White() {
		const FColor result = { 1.0f, 1.0f, 1.0f, 1.0f };
		return result;
	}

	FColor Red() {
		const FColor result = { 1.0f, 0.0f, 0.0f, 1.0f };
		return result;
	}

	FColor Green() {
		const FColor result = { 0.0f, 1.0f, 0.0f, 1.0f };
		return result;
	}

	FColor Blue() {
		const FColor result = { 0.0f, 0.0f, 1.0f, 1.0f };
		return result;
	}
}