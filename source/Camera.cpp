#include "Camera.h"

Camera::Camera(GLFWwindow* p_window) {
	glfwSetWindowUserPointer(p_window, this);

	auto mouseCallback = [](GLFWwindow* window, double xpos, double ypos)
	{
		static_cast<Camera*>(glfwGetWindowUserPointer(window))->mouse_callback(window, xpos, ypos);
	};

	auto scrollCallback = [](GLFWwindow* window, double xoffset, double yoffset)
	{
		static_cast<Camera*>(glfwGetWindowUserPointer(window))->scroll_callback(window, xoffset, yoffset);
	};

	glfwSetCursorPosCallback(p_window, mouseCallback);
	glfwSetScrollCallback(p_window, scrollCallback);
}

void Camera::moveForward(float p_deltaTime) {
	//glm::vec3 front = glm::vec3(m_cameraFront.x, 0, m_cameraFront.z); -> FPS Camera
	m_cameraPos += m_cameraFront * (m_speed * p_deltaTime);
}

void Camera::moveBackwards(float p_deltaTime) {
	//glm::vec3 back = glm::vec3(m_cameraFront.x, 0, m_cameraFront.z); -> FPS Camera
	m_cameraPos -= m_cameraFront * (m_speed * p_deltaTime);
}

void Camera::moveLeft(float p_deltaTime) {
	m_cameraPos -= glm::normalize(glm::cross(m_cameraFront, m_cameraUp)) * (m_speed * p_deltaTime);
}

void Camera::moveRight(float p_deltaTime) {
	m_cameraPos += glm::normalize(glm::cross(m_cameraFront, m_cameraUp)) * (m_speed * p_deltaTime);
}

glm::vec3 Camera::getDirection() {
	glm::vec3 direction;
	direction.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
	direction.y = sin(glm::radians(m_pitch));
	direction.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));

	return direction;
}

glm::vec3 Camera::getCameraFront() {
	m_cameraFront = glm::normalize(getDirection());
	return m_cameraFront;
}

glm::vec3 Camera::getCameraPosition() {
	return m_cameraPos;
}

glm::vec3 Camera::getCameraUp() {
	return m_cameraUp;
}

float Camera::getFov() {
	return m_fov;
}

glm::mat4 Camera::calculateView() {
	glm::mat4 view = glm::mat4(1.0f);
	// note that we're translating the scene in the reverse direction of where we want to move
	//view = glm::translate(view, glm::vec3(0.0f, 0.0f, -5.0f));
	view = glm::lookAt(m_cameraPos, m_cameraPos + getCameraFront(), m_cameraUp);

	return view;
}

glm::mat4 Camera::calculateProjection() {
	glm::mat4 projection = glm::mat4(1.0f);
	projection = glm::perspective(glm::radians(m_fov), (1280 * 1.0f) / (720 * 1.0f), 0.1f, 1000.0f);

	return projection;
}

void Camera::mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	float xoffset = xpos - m_lastX;
	float yoffset = ypos - m_lastY;
	m_lastX = xpos;
	m_lastY = ypos;

	const float sensitivity = 0.1f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	m_yaw += xoffset;
	m_pitch -= yoffset;

	if (m_pitch > 89.0f) {
		m_pitch = 89.0f;
	}
	if (m_pitch < -89.0f) {
		m_pitch = -89.0f;
	}
}

void Camera::scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	m_fov -= (float)yoffset;
	if (m_fov < 1.0f) {
		m_fov = 1.0f;
	}
	if (m_fov > 65.0f) {
		m_fov = 65.0f;
	}
}