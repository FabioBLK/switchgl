#include "Shader.h"

Shader::Shader(const char* vertexPath, const char* fragmentPath) {
	// 1. retrieve the vertex/fragment source code from filePath
	std::string vertexCode = getShaderCode(vertexPath);
	std::string fragmentCode = getShaderCode(fragmentPath);

	const char* vShaderCode = vertexCode.c_str();
	const char* fShaderCode = fragmentCode.c_str();

	// 2. compile shaders
	unsigned int vertex = compileVertexShader(vShaderCode);
	unsigned int fragment = compileFragmentShader(fShaderCode);

	std::vector<unsigned int> shaders = { vertex, fragment };
	ID = linkShader(shaders);

	// delete the shaders as they're linked into our program now and no longer necessary
	glDeleteShader(vertex);
	glDeleteShader(fragment);
}

Shader::Shader(const char* vertexPath, const char* fragmentPath, const char* geometryPath) {
	// 1. retrieve the vertex/fragment source code from filePath
	std::string vertexCode = getShaderCode(vertexPath);
	std::string fragmentCode = getShaderCode(fragmentPath);
	std::string geometryShader = getShaderCode(geometryPath);

	const char* vShaderCode = vertexCode.c_str();
	const char* fShaderCode = fragmentCode.c_str();
	const char* gShaderCode = geometryShader.c_str();

	// 2. compile shaders
	unsigned int vertex = compileVertexShader(vShaderCode);
	unsigned int fragment = compileFragmentShader(fShaderCode);
	unsigned int geometry = compileGeometryShader(gShaderCode);

	std::vector<unsigned int> shaders = { vertex, fragment, geometry };
	ID = linkShader(shaders);

	// delete the shaders as they're linked into our program now and no longer necessary
	glDeleteShader(vertex);
	glDeleteShader(fragment);
	glDeleteShader(geometry);
}

std::string Shader::getShaderCode(const char* path) {
	std::string shaderCode;
	std::ifstream shaderFile;

	// ensure ifstream objects can throw exceptions:
	shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try {
		// open files
		shaderFile.open(path);
		std::stringstream shaderStream;
		// read file's buffer contents into streams
		shaderStream << shaderFile.rdbuf();
		// close file handlers
		shaderFile.close();
		// convert stream into string
		shaderCode = shaderStream.str();
	}
	catch (std::ifstream::failure e) {
		FLogs::shared_instance().Log("ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ");
	}

	return shaderCode;
}

unsigned int Shader::compileVertexShader(const char* shaderCode) {
	int success;
	char infoLog[512];

	// vertex Shader
	unsigned int vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex, 1, &shaderCode, NULL);
	glCompileShader(vertex);
	// print compile errors if any
	glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertex, 512, NULL, infoLog);
		std::string infoStr = infoLog;
		std::string message = "ERROR::SHADER::VERTEX::COMPILATION_FAILED " + infoStr;
		FLogs::shared_instance().Log(message);
	}
	else {
		FLogs::shared_instance().Log("INFO::SHADER::Success compiling Vertex Shader");		
	}

	return vertex;
}

unsigned int Shader::compileFragmentShader(const char* shaderCode) {
	int success;
	char infoLog[512];

	// fragment Shader
	unsigned int fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment, 1, &shaderCode, NULL);
	glCompileShader(fragment);
	// print compile errors if any
	glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragment, 512, NULL, infoLog);
		std::string infoStr = infoLog;
		std::string message = "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED " + infoStr;
		FLogs::shared_instance().Log(message);
	}
	else {
		FLogs::shared_instance().Log("INFO::SHADER::Success compiling Fragment Shader");
	}

	return fragment;
}

unsigned int Shader::compileGeometryShader(const char* shaderCode) {
	int success;
	char infoLog[512];

	// fragment Shader
	unsigned int geometry = glCreateShader(GL_GEOMETRY_SHADER);
	glShaderSource(geometry, 1, &shaderCode, NULL);
	glCompileShader(geometry);
	// print compile errors if any
	glGetShaderiv(geometry, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(geometry, 512, NULL, infoLog);
		std::string infoStr = infoLog;
		std::string message = "ERROR::SHADER::GEOMETRY::COMPILATION_FAILED " + infoStr;
		FLogs::shared_instance().Log(message);
	}
	else {
		FLogs::shared_instance().Log("INFO::SHADER::Success compiling Geometry Shader");
	}

	return geometry;
}

unsigned int Shader::linkShader(std::vector<unsigned int> shaders) {
	int success;
	char infoLog[512];

	// Shader Program
	unsigned int shaderId = glCreateProgram();
	for (int i = 0; i < shaders.size(); i++) {
		glAttachShader(shaderId, shaders[i]);
	}
	glLinkProgram(shaderId);
	// print linking errors if any
	glGetProgramiv(shaderId, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderId, 512, NULL, infoLog);
		std::string infoStr = infoLog;
		std::string message = "ERROR::SHADER::PROGRAM::LINKING_FAILED " + infoStr;
		FLogs::shared_instance().Log(message);
	}

	return shaderId;
}

// use/activate the shader
void Shader::use() {
	glUseProgram(ID);
}

// utility uniform functions
void Shader::setBool(const std::string& name, bool value) const {
	unsigned int location = glGetUniformLocation(ID, name.c_str());
	glUniform1i(location, (int)value);
}

void Shader::setInt(const std::string& name, int value) const {
	unsigned int location = glGetUniformLocation(ID, name.c_str());
	glUniform1i(location, value);
}

void Shader::setFloat(const std::string& name, float value) const {
	unsigned int location = glGetUniformLocation(ID, name.c_str());
	glUniform1f(location, value);
}

void Shader::setVec4Float(const std::string& name, float valueR, float valueG, float valueB, float valueA) const {
	unsigned int location = glGetUniformLocation(ID, name.c_str());
	glUniform4f(location, valueR, valueG, valueB, valueA);
}

void Shader::setVec3Float(const std::string& name, float valueR, float valueG, float valueB) const {
	unsigned int location = glGetUniformLocation(ID, name.c_str());
	glUniform3f(location, valueR, valueG, valueB);
}

void Shader::setMat4Float(const std::string& name, const glm::mat4 matrix) const {
	unsigned int location = glGetUniformLocation(ID, name.c_str());
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::setUniformBlockInt(const std::string& name, const int value) const {
	unsigned int uniformBlockIndex = glGetUniformBlockIndex(ID, name.c_str());
	glUniformBlockBinding(ID, uniformBlockIndex, value);
}
