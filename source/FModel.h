#pragma once
#include "Shader.h"
#include "IModel.h"

#if defined(_M_X64) || defined(__amd64__)
#include "x64_platform/AssimpModel.h"
#endif

#include "TinyModel.h"

class FModel
{
public:
	FModel(std::string path, bool p_flipTexure);
	~FModel();
	IModel* Model();

private:
	IModel* m_model;
};