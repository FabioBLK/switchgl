#pragma once

#include "Shader.h"

#include <string>
#include <vector>
#include <glm/glm.hpp>

enum class MeshTextureType
{
	TEXTURE_DIFFUSE,
	TEXTURE_SPECULAR
};

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
};

struct Texture {
	unsigned int id;
	MeshTextureType type;
	std::string path;
};

struct Material {
	int id;
	glm::vec3 diffuseColor;
	glm::vec3 specularColor;
	float shineness;
	std::vector<Texture> textures;
};

class Mesh
{
public:
	// mesh data
	std::vector<Vertex> m_vertices;
	std::vector<unsigned int> m_indices;
	std::vector<Texture> m_textures;
	unsigned int m_materialIndex;

	Mesh(std::vector<Vertex> &vertices, std::vector<unsigned> &indices, unsigned int materialIndex);
	void Draw(Shader &shader, Material& material, unsigned int whiteTextureId, bool p_instancedDraw, const unsigned int p_amount);
	unsigned int GetVAO();

private:
	// render data
	unsigned int VAO;
	unsigned int VBO;
	unsigned int EBO;

	void setupMesh();
};

