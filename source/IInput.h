#pragma once
class IInput
{
public:
	virtual void MousePosition(double xpos, double ypos) = 0;
	virtual void MouseScroll(double xoffset, double yoffset) = 0;
	virtual ~IInput();
};