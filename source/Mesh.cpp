#include "Mesh.h"

Mesh::Mesh(std::vector<Vertex>& vertices, std::vector<unsigned>& indices, unsigned int materialIndex) {
	m_vertices = vertices;
	m_indices = indices;

	m_materialIndex = materialIndex;

	setupMesh();
}

void Mesh::Draw(Shader& shader, Material& material, unsigned int whiteTextureId, bool p_instancedDraw, const unsigned int p_amount) {
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;

	// ------------------------------------------------
	//std::cout << "texture count = " << material.textures.size() << std::endl;

	for (unsigned int i = 0; i < material.textures.size(); i++) {
		//glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE0 + i);// activate proper texture unit before binding
		// retrieve texture number (the N in diffuse_textureN)
		std::string number;
		MeshTextureType type = material.textures[i].type;
		std::string name;
		if (type == MeshTextureType::TEXTURE_DIFFUSE) {
			number = std::to_string(diffuseNr++);
			name = "diffuse";
		}
		else if (type == MeshTextureType::TEXTURE_SPECULAR) {
			number = std::to_string(specularNr++);
			name = "specular";
		}

		// TODO: Improve this texture set
		if (i > 0 && name == "diffuse") {
			break;
		}

		std::string property = "material." + name;// +number;
		shader.setInt(property, i);
		//std::cout << "DRAW::SETTING PROPERTY " << property << " to shader at index " << i << std::endl;
		glBindTexture(GL_TEXTURE_2D, material.textures[i].id);
		//std::cout << material.textures[i].path << " " << i << " " << name << std::endl;
	}

	if (material.textures.size() == 0) {
		shader.setInt("material.diffuse", 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, whiteTextureId);
	}

	if (material.id >= 0) {
		shader.setVec3Float("material.diffuseColor", material.diffuseColor.x, material.diffuseColor.y, material.diffuseColor.z);
		shader.setVec3Float("material.specularColor", material.specularColor.x, material.specularColor.y, material.specularColor.z);
		shader.setFloat("material.shineness", material.shineness);
		//std::cout << material.diffuseColor.x << " - " << material.diffuseColor.y << " - " << material.diffuseColor.z << std::endl;
	}

	glActiveTexture(GL_TEXTURE0);

	if (p_instancedDraw) {
		glBindVertexArray(VAO);
		glDrawElementsInstanced(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0, p_amount);
		glBindVertexArray(0);
	}
	else {
		// draw mesh
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
		//glDrawArrays(GL_TRIANGLES, 0, m_indices.size());
		glBindVertexArray(0);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Mesh::setupMesh() {
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(Vertex), &m_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(unsigned int), &m_indices[0], GL_STATIC_DRAW);

	// vertex position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

	// vertex normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));

	// vertex texture coordinate
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

	glBindVertexArray(0);
}

unsigned int Mesh::GetVAO() {
	return VAO;
}