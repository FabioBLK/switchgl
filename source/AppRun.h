#pragma once
#include "FLogs.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>

#include <glm/glm.hpp>
#include <map>

#include "Shader.h"
#include "Camera.h"
#include "VideoGL.h"
#include "TinyModel.h"
#include "Colors.h"
#include "FTime.h"

class AppRun
{
public:
	AppRun();
	void GettingStartedApp();
	void Lighting();
	void Models();
	void StencilTests();
	void BlendTests();
	void CubeMapTest();
	void AdvancedGLSL();
	void UniformBufferTest();
	void GeometryShaderInit();
	void NormalDebugModels();
	void InstancingTest();
	void AsteroidBelt();
	void AsteroidBeltInstance();
	bool getInput(GLFWwindow* p_window, int key);
};

