#pragma once
#include "Files.h"

#include <iostream>
#include <unordered_map>
#include <chrono>
#include <ctime>

enum class FLogType
{
	FDEBUG,
	FWARNING,
	FERROR
};

class FLogs
{
public:
	static FLogs& shared_instance() {
		static FLogs flog = FLogs(true);
		return flog;
	};
	
	void Log(std::string p_message, FLogType p_type = FLogType::FDEBUG);

private:
	FLogs();
	FLogs(bool p_logToFile);
	void LogToFile(std::string p_message);

	std::unordered_map<FLogType, std::string> m_typeStr = {
		{FLogType::FDEBUG, "DEBUG::"},
		{FLogType::FWARNING, "WARNING::"},
		{FLogType::FERROR, "ERROR::"},
	};

	std::string m_filePath;
	Files m_files;
	bool m_logToFile;
};