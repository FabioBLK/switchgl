#pragma once

#include "../FLogs.h"
#include "../Shader.h"
#include "../Mesh.h"
#include "../IModel.h"

#include <vector>
#include <unordered_map>
#include <string>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "../stb_image.h"

class AssimpModel : public IModel
{
public:
	AssimpModel(std::string path, bool p_flipTexure);
	virtual void DrawModel(Shader& shader) override;
	virtual void Draw(Shader& shader) override;
	virtual void DrawInstanced(Shader& shader, const unsigned int p_amount) override;
	virtual void SetInstances(const unsigned int p_amount, glm::mat4* p_matrices) override;
	virtual void SetPosition(const glm::vec3* p_position) override;
	virtual void SetScale(const glm::vec3* p_scale) override;
private:
	std::vector<Texture> textures_loaded;
	std::unordered_map<unsigned int, Material> m_materials_loaded;
	std::vector<Mesh> m_meshes;
	std::string m_directory;

	void loadModel(std::string path);
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, MeshTextureType meshType);
	unsigned int TextureFromFile(std::string path, std::string directory);
	unsigned int WhiteTexture();
	unsigned int m_whiteTextureId;
	bool m_flipTexture;
	glm::mat4 m_modelMatrix;
	glm::vec3 m_position;
	glm::vec3 m_scale;
};

