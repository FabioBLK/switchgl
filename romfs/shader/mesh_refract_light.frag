#version 330 core

struct Material {
	vec3 diffuseColor;
	vec3 specularColor;
	sampler2D diffuse;
	sampler2D specular;
	float shineness;
};

out vec4 FragColor;

// Vertex shader input
in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoord;

// Uniforms
uniform vec3 viewPos;
uniform Material material;
uniform samplerCube skybox;

float airRefract = 1.0;
float waterRefract = 1.33;
float iceRefract = 1.309;
float glassRefract = 1.52;
float diamondRefract = 2.42;

void main()
{
	// properties
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(viewPos - FragPos);

	float ratio = airRefract / waterRefract;
	
	vec3 I = normalize(FragPos - viewPos);
	vec3 R = refract(I, norm, ratio);

	vec3 result = vec3(0.0);

	vec4 refractColor = texture(skybox, R);
	FragColor = vec4(refractColor.rgb, 1.0);
};