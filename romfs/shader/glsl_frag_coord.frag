#version 330 core

out vec4 FragColor2;
uniform vec4 myColor1;
uniform vec4 myColor2;

void main()
{
	if (gl_FragCoord.x < 640) {
		FragColor2 = myColor1;
	}
	else {
		FragColor2 = myColor2;
	}
};