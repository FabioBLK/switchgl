#version 330 core

out vec4 FragColor;

in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;
uniform sampler2D ourTexture2;
uniform float alpha;
uniform vec3 myColor;
uniform vec3 myLight;

void main()
{
	float ambientStrength = 0.1;
	vec3 ambient = ambientStrength * myLight;
	vec3 result = ambient * myColor;

	FragColor = vec4(vec4(result, 1.0) * mix(
		texture(ourTexture, TexCoord), 
		texture(ourTexture2, vec2(TexCoord.x, TexCoord.y)),
		alpha
	));
};