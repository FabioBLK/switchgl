#version 330 core

out vec4 FragColor;

in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;
uniform sampler2D ourTexture2;
uniform float alpha;

void main()
{
	FragColor = vec4(vec4(ourColor, 1.0) * mix(
		texture(ourTexture, TexCoord), 
		texture(ourTexture2, vec2(TexCoord.x, TexCoord.y)),
		alpha
	));

	//FragColor = vec4(ourColor,1.0);
};