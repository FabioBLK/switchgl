#version 330 core

out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D ourTexture;

void main()
{
	float zColor = texture(ourTexture, TexCoord).a * -1.0;
	vec4 texColor = vec4(vec3(1.0 - texture(ourTexture, TexCoord)), -zColor);
    FragColor = texColor;
};