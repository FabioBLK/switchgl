#version 330 core

struct Material {
	vec3 diffuseColor;
	vec3 specularColor;
	sampler2D diffuse;
	sampler2D specular;
	float shineness;
};

struct DirLight {
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight {
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct SpotLight {
	vec3 position;
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float cutoff;
	float outerCutOff;
};

out vec4 FragColor;

// Vertex shader input
in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoord;


// Uniforms
uniform vec3 viewPos;
uniform Material material;
uniform DirLight dirLight;
uniform SpotLight spotLight;

#define NR_POINT_LIGHTS 1
uniform PointLight pointLights[NR_POINT_LIGHTS];

// Functions Prototypes
float LinearizeDepth(float depth);

float near = 0.1; 
float far  = 100.0;

void main()
{
	// properties
	float depth = LinearizeDepth(gl_FragCoord.z) / far;
	FragColor = vec4(vec3(depth), 1.0);
};

float LinearizeDepth(float depth) {
	float z = depth * 2.0 - 1.0;
	return (2.0 * near * far) / (far + near - z * (far - near));
}