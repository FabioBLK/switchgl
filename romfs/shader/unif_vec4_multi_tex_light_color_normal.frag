#version 330 core

out vec4 FragColor;

in vec3 ourColor;
in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPos;

uniform sampler2D ourTexture;
uniform sampler2D ourTexture2;
uniform float alpha;
uniform vec3 myColor;
uniform vec3 myLight;
uniform vec3 lightPos;
uniform vec3 viewPos;

void main()
{
	float ambientStrength = 0.1;
	vec3 ambient = ambientStrength * myLight;

	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(lightPos - FragPos);

	float diff = max(dot(norm, lightDir), 0.0);
	vec3 difuse = diff * myLight;

	float specularStrength = 1.0;
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);

	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 specular = specularStrength * spec * myLight;

	vec3 result = (ambient + difuse + specular) * myColor;

	FragColor = vec4(vec4(result, 1.0) * mix(
		texture(ourTexture, TexCoord), 
		texture(ourTexture2, vec2(TexCoord.x, TexCoord.y)),
		alpha
	));
};