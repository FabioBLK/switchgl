#version 330 core

struct Material {
	sampler2D diffuse;
	sampler2D specular;
	float shineness;
};

struct Light {
	vec3 position;
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float cutoff;
	float outerCutOff;
};

out vec4 FragColor;

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoord;

uniform Material material;
uniform Light light;

uniform vec3 viewPos;

void main()
{
	vec3 lightDir = normalize(light.position - FragPos);

	// ambient
	vec3 ambient = vec3(texture(material.diffuse, TexCoord)) * light.ambient;

	// light spot
	float tetha = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutoff - light.outerCutOff;
	float intensity = clamp((tetha - light.outerCutOff) / epsilon, 0.0, 1.0);

	// diffuse
	vec3 norm = normalize(Normal);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoord));

	// specular
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);

	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shineness);
	vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoord));
	
	diffuse *= intensity;
	specular *= intensity;
	
	vec3 result = (ambient + diffuse + specular);
	
	FragColor = vec4(result, 1.0);
};